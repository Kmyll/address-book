"use strict";

var Contacts = {
  index: window.localStorage.getItem("Contacts:index"),
  $table: document.getElementById("contacts-table"),
  $form: document.getElementById("contacts-form"),
  $save: document.getElementById("save"),
  $cancel: document.getElementById("cancel"),

  init: function () {
    // initialize the storage index
    if (!Contacts.index) {
      window.localStorage.setItem("Contacts:index", (Contacts.index = 1));
    }

    // initialize the form
    Contacts.$form.reset();
    Contacts.$cancel.addEventListener(
      "click",
      function (event) {
        Contacts.$form.reset();
        Contacts.$form.id_entry.value = 0;
      },
      true
    );
    Contacts.$form.addEventListener(
      "submit",
      function (event) {
        var entry = {
          id: parseInt(this.id_entry.value),
          name: this.name.value,
          surname: this.surname.value,
          phone: this.phone.value,
          address: this.address.value,
          city: this.city.value,
          zip: this.zip.value,
        };
        if (entry.id == 0) {
          // add
          Contacts.storeAdd(entry);
          Contacts.tableAdd(entry);
        } else {
          // edit
          Contacts.storeEdit(entry);
          Contacts.tableEdit(entry);
        }

        this.reset();
        this.id_entry.value = 0;
        event.preventDefault();
      },
      true
    );

    // initialize the table
    if (window.localStorage.length - 1) {
      var contacts_list = [],
        i,
        key;
      for (i = 0; i < window.localStorage.length; i++) {
        key = window.localStorage.key(i);
        if (/Contacts:\d+/.test(key)) {
          contacts_list.push(JSON.parse(window.localStorage.getItem(key)));
        }
      }

      if (contacts_list.length) {
        contacts_list
          .sort(function (a, b) {
            return a.id < b.id ? -1 : a.id > b.id ? 1 : 0;
          })
          .forEach(Contacts.tableAdd);
      }
      Contacts.$table.addEventListener(
        "click",
        function (event) {
          var op = event.target.getAttribute("data-op");
          if (/edit|remove/.test(op)) {
            var entry = JSON.parse(
              window.localStorage.getItem(
                "Contacts:" + event.target.getAttribute("data-id")
              )
            );
            if (op == "edit") {
              Contacts.$form.name.value = entry.name;
              Contacts.$form.surname.value = entry.surname;
              Contacts.$form.phone.value = entry.phone;
              Contacts.$form.address.value = entry.address;
              Contacts.$form.city.value = entry.city;
              Contacts.$form.zip.value = entry.zip;
              Contacts.$form.id_entry.value = entry.id;
            } else if (op == "remove") {
              if (
                confirm(
                  'Are you sure you want to remove "' +
                    entry.name +
                    " " +
                    entry.surname +
                    '" from your contacts?'
                )
              ) {
                Contacts.storeRemove(entry);
                Contacts.tableRemove(entry);
              }
            }
            event.preventDefault();
          }
        },
        true
      );
    }
  },

  storeAdd: function (entry) {
    entry.id = Contacts.index;
    window.localStorage.setItem("Contacts:" + entry.id, JSON.stringify(entry));
    window.localStorage.setItem("Contacts:index", ++Contacts.index);
  },
  storeEdit: function (entry) {
    window.localStorage.setItem("Contacts:" + entry.id, JSON.stringify(entry));
  },
  storeRemove: function (entry) {
    window.localStorage.removeItem("Contacts:" + entry.id);
  },

  tableAdd: function (entry) {
    var $tr = document.createElement("tr"),
      $td,
      key;
    $tr.className += "tableRow";
    for (key in entry) {
      if (entry.hasOwnProperty(key)) {
        $td = document.createElement("td");
        $td.className += " tableData";
        $td.appendChild(document.createTextNode(entry[key]));
        $tr.appendChild($td);
      }
    }
    $td = document.createElement("td");

    $td.innerHTML =
      '<a class="edit" data-op="edit" data-id="' +
      entry.id +
      '">Edit</a> | <a class="remove" data-op="remove" data-id="' +
      entry.id +
      '">Remove</a>';
    $tr.appendChild($td);
    $tr.setAttribute("id", "entry-" + entry.id);
    Contacts.$table.appendChild($tr);
  },
  tableEdit: function (entry) {
    var $tr = document.getElementById("entry-" + entry.id),
      $td,
      key;
    $tr.innerHTML = "";
    for (key in entry) {
      if (entry.hasOwnProperty(key)) {
        $td = document.createElement("td");

        $td.appendChild(document.createTextNode(entry[key]));
        $tr.appendChild($td);
      }
    }
    $td = document.createElement("td");

    $td.innerHTML =
      '<a class="edit"  data-op="edit" data-id="' +
      entry.id +
      '">Edit</a> | <a  class="remove" data-op="remove" data-id="' +
      entry.id +
      '">Remove</a>';
    $tr.appendChild($td);
  },
  tableRemove: function (entry) {
    Contacts.$table.removeChild(document.getElementById("entry-" + entry.id));
  },
};
Contacts.init();

console.log(Contacts.$table.innerText);

//FILTER

function searchTable() {
  var input, filter, found, table, tr, td, i, j;
  input = document.getElementById("SearchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("contacts-table");

  tr = table.getElementsByClassName("tableRow");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td");
    for (j = 0; j < td.length; j++) {
      if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
        found = true;
      }
    }
    if (found) {
      tr[i].style.display = "";

      found = false;
    } else {
      tr[i].style.display = "none";
    }
  }
}

//JQUERY

$(document).ready(function () {

  $("#save").css({
    backgroundColor: "#7abfc5",
    width: "70px",
    height: "30px",
    borderRadius: "5px",
    border: "1px solid lightgrey",
    marginLeft: "6%",
    fontSize: '14px',
    color: "white",
  });
  $("#cancel").css({
    color: "#7abfc5",
    width: "70px",
    height: "30px",
    borderRadius: "5px",
    border: "1px solid #7abfc5",
    marginRight: "6%",
    fontSize: '14px'
  });
  $(".tableRow:even").css({ background: "F3F3F3" });

  $(".add").on("click", function () {
    $(".modal").show();
    $(".modal").css({
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    });
  });
  $(".edit").on("click", function () {
    $(".modal").fadeIn();
    $(".modal").css({
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    });
  });
  $(".close").on("click", function () {
    $(".modal").fadeOut();
    location.reload();
  });
  $("#save").on("click", function () {
    $(".modal").fadeOut();
  });
  $("input").focus(function () {
    $(this).css("background", "#ede7f6");
  });
  
  $("#SearchInput").focus(function () {
    $(this).css("background", "white");
  });

  $("input").blur(function () {
    $(this).css("background", "none");
  });
});
